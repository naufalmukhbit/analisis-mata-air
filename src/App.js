import { createRef, useRef, useEffect, useState } from "react";
import "./App.css";
import { Container, Nav, Navbar, Button } from "react-bootstrap";
import Home, {
  Analisis,
  GambaranUmumWilayah,
  Kesimpulan,
  LatarBelakang,
  Metodologi,
} from "./components";
import { HomeIcon } from "./components/UI";

function App() {
  const refs = Array(6).fill().map(item => createRef(null));
  var refsOffset = useRef([]);

  const [active, setActive] = useState("home");

  const handleClick = (ref) => {
    ref.current.scrollIntoView({
      behavior: "smooth"
    })
  }

  const handleScroll = () => {
    let scrollValue = window.pageYOffset;
    let scrollHeight = window.innerHeight / 2;
    
    if (scrollValue >= (refsOffset.current[5] - scrollHeight)) {
      setActive("kesimpulan")
    } else if (scrollValue >= (refsOffset.current[4] - scrollHeight)) {
      setActive("analisis")
    } else if (scrollValue >= (refsOffset.current[3] - scrollHeight)) {
      setActive("gambaran")
    } else if (scrollValue >= (refsOffset.current[2] - scrollHeight)) {
      setActive("metodologi")
    } else if (scrollValue >= (refsOffset.current[1] - scrollHeight)) {
      setActive("latarbelakang")
    } else {
      setActive("home")
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
  })

  useEffect(() => {
    refsOffset.current = refs.map(item => item.current?.offsetTop)
  }, [refs])
  
  return (
    <>
      <Navbar expand="lg" bg="light" variant="light" fixed="top" style={{boxShadow: "rgba(0, 0, 0, 0.1) 0 0 25px, rgba(0, 0, 0, 0.04) 0 0 10px"}}>
        <Navbar.Toggle />
        <Navbar.Collapse>
          <Container style={{display: "flex", justifyContent: "space-between"}}>
            <div onClick={() => handleClick(refs[0])} className="navbar-item">
              <HomeIcon />
            </div>
            <Nav fill activeKey={active} style={{display: "flex", justifyContent: "space-between", width: "64%"}}>
              <Nav.Link eventKey="latarbelakang" onClick={() => handleClick(refs[1])}>Latar Belakang</Nav.Link>
              <Nav.Link eventKey="metodologi" onClick={() => handleClick(refs[2])}>Metodologi</Nav.Link>
              <Nav.Link eventKey="gambaran" onClick={() => handleClick(refs[3])}>Gambaran Umum Wilayah</Nav.Link>
              <Nav.Link eventKey="analisis" onClick={() => handleClick(refs[4])}>Analisis</Nav.Link>
              <Nav.Link eventKey="kesimpulan" onClick={() => handleClick(refs[5])}>Kesimpulan</Nav.Link>
            </Nav>
            <a href={`${process.env.PUBLIC_URL}/webgis_assets/index.html`}>
              <Button>Visualisasi</Button>
            </a>
          </Container>
        </Navbar.Collapse>
        
      </Navbar>
      <Container>
        <Home ref={refs[0]} />
        <LatarBelakang ref={refs[1]} />
        <Metodologi ref={refs[2]} />
        <GambaranUmumWilayah ref={refs[3]} />
        <Analisis ref={refs[4]} />
        <Kesimpulan ref={refs[5]} />
      </Container>
    </>
  );

  
}

export default App;
