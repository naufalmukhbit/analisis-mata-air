import { forwardRef } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Section, Title } from "./UI";

import Image1 from "../assets/images/analisis penggunaan lahan.PNG";
import Chart1 from "../assets/images/analisis penggunaan lahan graph.png";
import Table1 from "../assets/images/analisis penggunaan lahan tabel.png";

import Image2 from "../assets/images/analisis Penyedia Pengguna.png";
import Chart2 from "../assets/images/analisis Penyedia Pengguna graph.png";
import Table2 from "../assets/images/analisis Penyedia Pengguna tabel.png";

import Image3 from "../assets/images/analisis penggunaan lahan Penyedia Pengguna.PNG";
import Chart3 from "../assets/images/analisis penggunaan lahan Penyedia Pengguna graph.png";
import Table3 from "../assets/images/analisis penggunaan lahan Penyedia Pengguna tabel.png";

const Analisis = forwardRef((props, ref) => {
  const dataStyle = {
    display: "flex",
    alignItems: "center",
    height: "25rem",
    justifyContent: "space-around",
    paddingTop: "5rem"
  }
  return (
    <Container fluid ref={ref} style={{paddingTop: "56px"}}>
      <Title>Analisis</Title>
      <Section title="Penggunaan Lahan">
        <Row>
          <Col
            xs={{ span: 12, order: "last" }}
            lg={{ span: 5, order: "first" }}
          >
            <p align="justify">
              Lahan terbangun dan tegalan/ladang merupakan dua wilayah yang paling
              luas menutupi daerah lokasi kajian dengan keduanya menutupi
              sekitar 52% total luas penggunaan lahan. Sementara itu luasan
              hutan hanya ada sekitar 8.47% atau sekitar 4533 Ha saja. Hal ini
              mengindikasikan Kawasan bandung utara yang seharusnya menjadi
              wilayah konservasi air, sudah mulai kritis kondisinya dengan
              banyaknya lahan terbangun maupun area terbuka yang bukan vegetasi
              hutan, sehingga dibutuhkan segera revegetasi Kembali untuk
              meningkatkan Kembali daya dukung jasa ekosistemnya.
            </p>
          </Col>
          <Col
            xs={{ span: 12, order: "first" }}
            lg={{ span: 7, order: "last" }}
          >
            <img
              src={Image1}
              alt="Penggunaan lahan"
              style={{ width: "100%" }}
            />
          </Col>
        </Row>
        <div style={dataStyle}>
          <img src={Chart1} alt="Chart penggunaan Lahan" style={{maxWidth: "100%", height: "100%"}} />
          <img src={Table1} alt="Tabel penggunaan Lahan" style={{maxWidth: "60%", height: "60%"}} />
        </div>
      </Section>
      <Section title="Daerah Penyedia/Pengguna Air">
        <Row>
          <Col md={12} lg={7}>
            <img
              src={Image2}
              alt="Daerah penyedia/pengguna air"
              style={{ width: "100%" }}
            />
          </Col>
          <Col md={12} lg={5}>
            <p align="justify">
              Berdasarkan hasil analisis, Daerah penyedia air memiliki cakupan
              wilayah sebesar 2127 Ha yang mayoritas berada di sebelah utara
              Kota Bandung yaitu di Kabupaten Bandung dan Sebagian utara Kota
              cimahi maupun Kabupaten Bandung Barat. Sedangkan luasan daerah
              pengguna air yaitu sebesar 4372 Ha yang Sebagian besarnya berada
              di Kota Bandung. Dari sini dapat disimpulkan bahwa Kota Bandung
              merupakan daerah yang paling banyak menggunakan mata air yang
              berasal dari Kawasan bandung barat.
            </p>
          </Col>
        </Row>
        <div style={dataStyle}>
          <img src={Chart2} alt="Chart daerah penyedia/pengguna air" style={{maxWidth: "100%", height: "100%"}} />
          <img src={Table2} alt="Tabel daerah penyedia/pengguna air" style={{maxWidth: "35%", height: "25%"}} />
        </div>
      </Section>
      <Section title="Penggunaan Lahan di Daerah Penyedia/Pengguna Air">
        <Row>
          <Col
            xs={{ span: 12, order: "last" }}
            lg={{ span: 5, order: "first" }}
          >
            <p align="justify">
              Dari hasil analisis, pada daerah penyedia air, lahan terbangun
              masih mendominasi penggunaan lahan dengan persentase luas 11.53%,
              sedangkan luasan hutan hanya sekitar 0.898% atau 58.29 Ha. Belum
              lagi ditambah luasan penggunaan lahan sawah, perkebunan dan
              tegalan yang cukup luas juga tentunya semakin menekan kemampuan
              daerah penyedia air di Kawasan bandung utara. Sedangkan pada
              daerah pengguna air, dapat diperhatikan, penggunaan air dari mata
              air di KBU ini mayoritas digunakan oleh pemukiman/industry sebagai
              lahan terbangun dan persawahan. Artinya sumberdaya air dari 13
              titik mata air di KBU banyak dimanfaatkan oleh masyarakat, dengan
              begitu ekosistem lingkungan di aderah penyedia air harus dijaga
              seupaya tidak terjadi kerusakan/penurunan jasa ekosistem air.
            </p>
          </Col>
          <Col
            xs={{ span: 12, order: "first" }}
            lg={{ span: 7, order: "last" }}
          >
            <img
              src={Image3}
              alt="Penggunaan lahan di daerah penyedia/pengguna air"
              style={{ width: "100%" }}
            />
          </Col>
        </Row>
        <div style={dataStyle}>
          <img src={Chart3} alt="Chart penggunaan lahan di daerah penyedia/pengguna air" style={{maxWidth: "100%", height: "100%"}} />
          <img src={Table3} alt="Tabel penggunaan lahan di daerah penyedia/pengguna air" style={{maxWidth: "100%", height: "100%"}} />
        </div>
      </Section>
    </Container>
  );
})

export default Analisis