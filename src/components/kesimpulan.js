import { forwardRef } from "react";
import { Container, Row, Col, Card } from "react-bootstrap"
import { Title } from "./UI";

const Kesimpulan = forwardRef((props, ref) => {
  const cardStyle = {
    minHeight: "240px",
    boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
    marginBottom: "2rem",
    borderRadius: "16px"
  }

  return (
    <Container fluid ref={ref} style={{paddingTop: "56px", minHeight: "100vh"}}>
      <div style={{ textAlign: "center" }}>
        <Title>Kesimpulan</Title>
      </div>
      <Row>
        <Col xs={12} md={6}>
          <Card body style={cardStyle}>
            Berdasarkan hasil analisis, di Kawasan bandung utara sudah ulai
            terjadi kerusakan ekosistem dengan semakin banyaknya lahan terbangun
            dan area persawahan sementara Kawasan hutan semakin mengecil.
          </Card>
        </Col>
        <Col xs={12} md={6}>
          <Card body style={cardStyle}>
            Keberadaan hutan di daerah penyedia air pun semakin minim, sehingga
            terdapat potensi kerusakan sumber mata air di KBU.
          </Card>
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={6}>
          <Card body style={cardStyle}>
            Potensi air yang dihasilkan dari 13 titik mata air di KBU sebenarnya
            sangat potensial dan banyak digunakan masyarakat, untuk itu
            keberadaan ekosistem hutan sebagai area resapan di daerah penyedia
            air perlu dijaga agar sumberdaya air tetap berkelanjutan.
          </Card>
        </Col>
        <Col xs={12} md={6}>
          <Card body style={cardStyle}>
            Melihat dari hasil analisis spasial, Kota Bandung merupakan daerah
            yang paling luas dalam memanfaatkan sumber air ini, sehingga
            pembayaran jasa ekosistem mayoritas dapat dilakukan oleh Kota
            Bandung, sementara daerah yang terdapat wilayah penyedia air seperti
            daerah Utara Kota Bandung, Kabupaten Bandung Barat dan Sebagian Kota
            Cimahi wajib untuk menjaga kelangsungan ekosistem hutan demi menjaga
            keberlanjutan sumberdaya air ini.
          </Card>
        </Col>
      </Row>
    </Container>
  );
})

export default Kesimpulan