import { Title } from "./UI";
import { Container, Row, Col } from "react-bootstrap";
import Wilayah from "../assets/images/Deskripsi Wilayah.PNG";
import { forwardRef } from "react";

const GambaranUmumWilayah = forwardRef((props, ref) => {
  return (
    <Container fluid ref={ref} style={{paddingTop: "56px", minHeight: "100vh"}}>
      <Title>Gambaran Umum Wilayah</Title>
      <Row>
        <Col xs={12} lg={6}>
          <img
            src={Wilayah}
            alt="Deskripsi wilayah"
            style={{ width: "100%" }}
          />
        </Col>
        <Col xs={12} lg={6}>
          <p align="justify">
            Gambar di samping menunjukkan lokasi penelitian ini. Penelitian ini
            dilakukan di 13 titik mata air yang berada di kawsan bandung utara
            dengan cakupan wilayah analisis daerah penyedia/pengguna airnya
            berada di sekitar Kota Bandung dan Kota Cimahi. Penggunaan lahan
            yang akan dianalisis mencakup seluruh area kajian yaitu luasan KBU
            beserta wilayah Kota Bandung dan Kota Cimahi.
          </p>
        </Col>
      </Row>
    </Container>
  );
})

export default GambaranUmumWilayah