import { Section, Title } from "./UI";
import { Container, Row, Col } from "react-bootstrap";
import Metode from "../assets/images/Metode.PNG";
import { forwardRef } from "react";

const Metodologi = forwardRef((props, ref) => {
  return (
    <Container fluid ref={ref} style={{paddingTop: "56px"}}>
      <Title>Metodologi</Title>
      <Section title="Bahan dan Alat" small>
        <p align="justify">
          Bahan yang digunakan adalah citra SRTM (Shuttle Radar Topography
          Mission), peta tematik (peta Kawasan hutan, penutupan/pengunaan lahan
          dan peta administraai wilayah). Alat yang digunakan adalah GPS (Global
          Positioning System) untuk mengukur koordinat titik mata air serta
          perangkat lunak Global Mapper dan QGIS untuk analisis spasial.
        </p>
      </Section>
      <Section title="Cara Kerja" small>
        <Row>
          <Col
            xs={{ span: 12, order: "last" }}
            lg={{ span: 5, order: "first" }}
          >
            <p align="justify">
              Metode pembuatan peta jasa ekosistem air atau HESSA yang dibangun
              dilakukan melalui tahapan kerja sebagaimana disajikan pada Gambar
              dibawah. Gambar tersebut menunjukkan bahwa ada 2 kegiatan utama
              dalam pembuatan peta jasa ekosistem air, yaitu:
            </p>
            <ol type="a">
              <li>melakukan pengukuran lapangan</li>
              <li>analisis data spasial</li>
            </ol>
            <p align="justify">
              Pengukuran lapangan dilakukan untuk mendapatkan data titik
              koordinat titik mata air. Adapun analisis data spasial dilakukan
              untuk memetakan wilayah penyedia air dan wilayah pengguna air yang
              ditentukan berdasarkan titik koordinat sumber mata air yang diukur
              di lapangan. Dalam analisis spasial untuk pemetaan jasa ekosistem
              air dibutuhkan data DEM (Digital Elevation Model) dari citra SRTM
              (Shuttle Radar Topography Mission). Pengolahan citra SRTM
              dilakukan dengan perangkat lunak Global Mapper melalui feature
              Terrain Analysis – Generate Watershed sehingga didapatkan batas
              Wilayah Penyedia/Pengguna Air. Batas Wilayah Penyedia Air
              merupakan wilayah yang dibatasi oleh topografi alami berupa
              punggung bukit yang aliran airnya keluar sebagai titik mata air.
              Adapun batas Wilayah Pengguna Air merupakan wilayah sepanjang
              aliran air mulai dari titik mata air sebagai titik awalnya sampai
              titik akhir aliran airnya. Di dalam delineasi batas Wilayah
              Penyedia/Pengguna Air harus memperhatikan garis kontur yang dibuat
              dengan feature Terrain Analysis – Generate Countours. Setelah
              mendapatkan batas Wilayah Penyedia/Pengguna Air, ditumpangsusunkan
              (overlay) dengan peta-peta tematik lainnya untuk mendapatkan
              informasi spasial sesuai yang dibutuhkan. Dalam penelitian ini,
              Wilayah Penyedia/Pengguna Air ditumpangsusunkan dengan peta
              Penggunaan lahan dan peta administrasi Provinsi Jawa Barat tahun.
            </p>
          </Col>
          <Col
            xs={{ span: 12, order: "first" }}
            lg={{ span: 7, order: "last" }}
          >
            <img
              src={Metode}
              alt="Metode penelitian"
              style={{ width: "100%" }}
            />
          </Col>
        </Row>
      </Section>
    </Container>
  );
})

export default Metodologi