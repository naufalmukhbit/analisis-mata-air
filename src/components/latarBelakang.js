import { forwardRef } from "react";
import LatBel from "../assets/images/latbel.jpg"
import { Container, Row, Col } from "react-bootstrap";
import { Title } from "./UI";

const LatarBelakang = forwardRef((props, ref) => {
  return (
    <Container fluid ref={ref} style={{paddingTop: "56px"}}>
      <Title>Latar Belakang</Title>
      <Row>
        <Col xs={{ span: 12, order: "last" }} lg={{ span: 5, order: "first" }}>
          <p align="justify">
            Sumberdaya air merupakan salah satu sumberdaya yang penting dan krusial
            bagi hajat hidup semua orang. Untuk itu, isu lingkungan mengenai air
            sudah menjadi fokus dan perhatian utama bagi banyak kalangan. Kawasan
            Bandung Utara (KBU) merupakan daerah yang telah ditetapkan sebagai
            daerah konservasi air. Curah hujan yang cukup tinggi, yaitu sekitar
            ±1500 – 3500 mm/tahun, dan batuan yang didominasi oleh batuan vulkanik
            menjadikan Kawasan Bandung Utara sebagai kawasan resapan air yang baik.
            Akan tetapi, pertumbuhan penduduk yang pesat tentunya menjadi
            kekhawatiran tersendiri karena dapat menekan keberadaan kawasan
            konservasi air itu sendiri.
          </p>
        </Col>
        <Col xs={{ span: 12, order: "first" }} lg={{ span: 7, order: "last" }}>
          <img src={LatBel} alt="Latar Belakang" style={{maxWidth: "100%"}} />
        </Col>
      </Row>
      <br /> 
      <p align="justify">
        Kontinuitas, kuantitas, dan kualitas air yang baik dihasilkan dari
        ekosistem hutan yang ekosistemnya baik, sebaliknya apabila ekosistem
        hutan terdegradasi maka jasa ekosistem air terganggu pula. Untuk
        menjamin keberlanjutan aliran air yang dihasilkan oleh ekosistem hutan,
        maka pelestarian hutan harus dilakukan. Pelestarian hutan sebagai
        penyedia air tidak hanya menjadi tanggung-jawab pengelola hutan tetapi
        juga tanggungjawab pengguna air sebagai penerima manfaatnya.
      </p>
      <br/>
      <p align="justify">
        Mekanisme imbal balik dari pengguna air untuk pelestarian ekosistem
        hutan sebagai daerah tangkapan airnya (DTA) dikenal sebagai PES (Payment
        for Ecosystem Services) atau pembayaran jasa ekosistem (PJE) yang saat
        ini mulai banyak dirintis dan diimplementasikan sebagai salah satu
        instrumen ekonomi lingkungan. Melalui mekanisme PJE tersebut, maka
        pemanfaat jasa ekosistem akan berkontribusi terhadap pelestarian
        ekosistem penyedia jasa ekosistemnya. Kawasan hutan di Provinsi Jawa
        Barat khususnya di Kawasan Bandung Utara (KBU), umumnya berada di bagian
        hulu daerah aliran sungai (DAS) yang berperan penting sebagai daerah
        tangkapan air (water catchment area) dan menyediakan jasa ekosistem air.
        Sumber mata air yang keluar ke bagian hilir dimanfaatkan oleh masyarakat
        untuk kebutuhan air rumah tangga, pertanian, industri, dan kegiatan
        lainnya. Pelestarian ekosistem hutan sudah seharusnya dilakukan oleh
        pengguna air di bagian hilirnya. Dari berbagai inisiatif PES yang saat
        ini sedang dikembangkan ditemukan permasalahan batas wilayah yang tidak
        jelas antara wilayah penyedia jasa ekosistem air dengan wilayah pengguna
        airnya. Belum adanya metode pemetaan jasa ekosistem yang praktis untuk
        menentukan batas wilayah-batas wilayah ekosistem penyedia air dan
        wilayah pengguna air sering menjadi kendala proses PJE. Oleh karena itu
        keberadaan peta jasa ekosistem air yang mampu mendelineasi batas antara
        wilayah penyedia air dan wilayah pengguna air dinilai menjadi penting
        dalam pengembangan mekanisme PJE tersebut. Penelitian ini bertujuan
        untuk menentukan pendekatan spasial dalam pemetaan wilayah penyedia dan
        pengguna air di Kawasan hutan pegunungan.
      </p>
    </Container>
  );
})

export default LatarBelakang
