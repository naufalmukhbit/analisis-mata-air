import { forwardRef } from "react";
import { Row, Col } from "react-bootstrap";
import TitleImage from "../assets/images/Judul.PNG"

import Analisis from "./analisis";
import GambaranUmumWilayah from "./gambaranWilayah";
import Kesimpulan from "./kesimpulan";
import LatarBelakang from "./latarBelakang";
import Metodologi from "./metodologi";

const Home = forwardRef((props, ref) => {
  return (
    <Row style={{height: "100vh"}}>
      <img src={TitleImage} alt="Title" style={{position: "absolute", minWidth: "100%", left: 0, height: "100%", filter: "brightness(0.5)"}} />
      <Col xs={12}>
        <div ref={ref} style={{
          height: "100%",  
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          padding: "112px 0",
        }}>
          <h1>
            <b style={{textShadow: "rgb(255 255 255 / 20%) 0px 2px 18px", color: "white"}}>Titik Mata Air di Kawasan Bandung Utara dan Daerah Penyedia/Pengguna Air-nya di Wilayah Kota Bandung dan Cimahi</b>
          </h1>
        </div>
      </Col>
    </Row>
    
  )
})
export default Home

export {
  Analisis,
  GambaranUmumWilayah,
  Kesimpulan,
  LatarBelakang,
  Metodologi,
};


